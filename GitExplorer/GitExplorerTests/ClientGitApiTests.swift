//
//  ClientGitApiTests.swift
//  GitExplorer
//
//  Created by Rafael Chei on 6/1/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import XCTest

@testable import GitExplorer

class ClientGitApiTests: XCTestCase {
    
    func testGetRepositories() {
        
        ClientGitApi.getRepositories(1)
        {(result: Repository?) in
            XCTAssertTrue(result!.items.count > 0)
        }
    }
    
    func testGetRepositoriesWithInvalidPage() {
        
        ClientGitApi.getRepositories(-1)
        {(result: Repository?) in
            XCTAssertTrue(result!.items.count == 0)
        }
    }
    
    func testgetGithubPullRequests() {
        ClientGitApi.getPullRequest("https://api.github.com/repos/facebook/react-native/pulls")
        {(result: [PullRequest]) in
            XCTAssertTrue(result.count > 0)
        }
    }
    
    func testgetOneGithubPullRequests() {
        ClientGitApi.getPullRequest("https://api.github.com/repos/facebook/react-native/pulls/7884")
        {(result: [PullRequest]) in
            XCTAssertTrue(result.count == 1)
        }
    }
    
    func testInvalidGetGithubPullRequests() {
        ClientGitApi.getPullRequest("https://api.github.com/repos/facebook/react-native/pulls/1111")
        {(result: [PullRequest]) in
            XCTAssertTrue(result.count == 0)
        }
    }
}
