//
//  MenuTableViewController.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit
import AMSlideMenu

class MenuTableViewController: AMSlideMenuLeftTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.backgroundView = nil
        self.tableView.backgroundColor = UIColor.backgroundMenuColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
