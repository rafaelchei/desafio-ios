//
//  RepoTableViewCell.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell {

    @IBOutlet weak var labelNomeRepo: UILabel!
    @IBOutlet weak var labelDescricaoRepo: UILabel!
    @IBOutlet weak var labelForkNumber: UILabel!
    @IBOutlet weak var labelStarNumber: UILabel!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageUser: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
