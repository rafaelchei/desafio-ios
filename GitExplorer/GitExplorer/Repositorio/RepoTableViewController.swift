//
//  RepoTableViewController.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit
import SDWebImage



class RepoTableViewController: UITableViewController {

    var repos : [Item]!
    var filteredRepos = [Item]()
    var searchController = UISearchController(searchResultsController: nil)
    var page: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Github JavaPop"
        
        repos = [Item]()
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.placeholder = "Buscar Repositorios"
        self.definesPresentationContext = true
        self.searchController.dimsBackgroundDuringPresentation = false
        self.tableView.tableHeaderView = self.searchController.searchBar
 
        
        tableView.infiniteScrollIndicatorStyle = .Gray
        self.getGithubRepo()

        tableView.addInfiniteScrollWithHandler { (scrollView) -> Void in
            let tableView = scrollView as! UITableView
            self.page = self.page + 1
            self.getGithubRepo(self.page)

            tableView.finishInfiniteScroll()
        }
        
    }

    func getGithubRepo() -> Void {
        getGithubRepo(self.page)
    }

    func getGithubRepo(page:Int) -> Void {
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.label.text = "Buscando Repositórios..."
        ClientGitApi.getRepositories(page)
        {(result: Repository?) in
            if(result?.items.count > 0)
            {
                self.repos.appendContentsOf(result!.items)
                self.tableView.reloadData()
                progressHUD.hideAnimated(true)
            }
        }
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return filteredRepos.count
        }
        return repos.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RepoCell", forIndexPath: indexPath) as! RepoTableViewCell
        
        var repositorio: Item
        if searchController.active && searchController.searchBar.text != "" {
            repositorio = filteredRepos[indexPath.row]
        } else {
            repositorio = repos[indexPath.row]
        }
        
        cell.labelNomeRepo.text = repositorio.name
        cell.labelDescricaoRepo.text = repositorio.descriptionField
        cell.labelForkNumber.text = "\(repositorio.forksCount)"
        cell.labelStarNumber.text = "\(repositorio.stargazersCount)"
        cell.labelUsername.text = repositorio.owner.login
        cell.labelName.text = repositorio.fullName
        
        cell.imageUser.layer.cornerRadius = cell.imageUser.frame.size.width / 2
        cell.imageUser.clipsToBounds = true
        SDWebImageManager.sharedManager().downloadImageWithURL(NSURL(string: repositorio.owner.avatarUrl),
            options: [],
            progress: nil,
            completed: {(image, error, cached, finished, url) in
            cell.imageUser.image = image
        })

        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let repo: Item
                if searchController.active && searchController.searchBar.text != "" {
                    repo = filteredRepos[indexPath.row]
                } else {
                    repo = repos[indexPath.row]
                }
                
                let controller = segue.destinationViewController as! PullRequestTableViewController
                controller.repositorio = repo
            }
        }
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredRepos = repos.filter { repo in
            return repo.name.lowercaseString.containsString(searchText.lowercaseString)
        }
        tableView.reloadData()
    }
}

extension RepoTableViewController: UISearchResultsUpdating {
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
}

extension RepoTableViewController: UISearchBarDelegate {
    func searchBar(searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: searchBar.scopeButtonTitles![selectedScope])
    }
}
