//
//  UIColorExtension.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit

extension UIColor {
    static func baseColor() -> UIColor {
        let rgb = getConfigurationColor("baseColor")
        return UIColor(red: rgb.red, green: rgb.green, blue: rgb.blue, alpha: 1.0)
    }
    static func backgroundMenuColor() -> UIColor {
        let rgb = getConfigurationColor("backgroundMenuColor")
        return UIColor(red: rgb.red, green: rgb.green, blue: rgb.blue, alpha: 1.0)
    }
    
    static func getConfigurationColor(configurationName: String) -> (red: CGFloat, green: CGFloat, blue: CGFloat)
    {
        let path = NSBundle.mainBundle().pathForResource("Theme", ofType: "plist")
        let dict = NSDictionary(contentsOfFile: path!) as? [String: AnyObject]
        let completeColor = dict![configurationName] as! NSString
        let completeColorString = completeColor as String
        let rgb = completeColorString.characters.split{$0 == ";"}.map(String.init)
        
        let red = Double(rgb[0])
        let green = Double(rgb[1])
        let blue = Double(rgb[2])
        
        let r = CGFloat(red!)
        let g = CGFloat(green!)
        let b = CGFloat(blue!)
        
        return (CGFloat(r/255.0), CGFloat(g/255.0), CGFloat(b/255.0))
    }
}