//
//	PullRequest.swift
//
//	Create by Rafael Chei on 31/5/2016
//	Copyright © 2016 Conductor. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


public class PullRequest : NSObject, NSCoding{

	var assignee : AnyObject!
	var base : Base!
	var body : String!
	var closedAt : AnyObject!
	var commentsUrl : String!
	var commitsUrl : String!
	var createdAt : String!
	var diffUrl : String!
	var head : Base!
	var htmlUrl : String!
	var id : Int!
	var issueUrl : String!
	var locked : Bool!
	var mergeCommitSha : String!
	var mergedAt : AnyObject!
	var milestone : AnyObject!
	var number : Int!
	var patchUrl : String!
	var reviewCommentUrl : String!
	var reviewCommentsUrl : String!
	var state : String!
	var statusesUrl : String!
	var title : String!
	var updatedAt : String!
	var url : String!
	var user : Owner!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		assignee = json["assignee"].stringValue
		let baseJson = json["base"]
		if baseJson != JSON.null{
			base = Base(fromJson: baseJson)
		}
		body = json["body"].stringValue
		closedAt = json["closed_at"].stringValue
		commentsUrl = json["comments_url"].stringValue
		commitsUrl = json["commits_url"].stringValue
		createdAt = json["created_at"].stringValue
		diffUrl = json["diff_url"].stringValue
		let headJson = json["head"]
		if headJson != JSON.null{
			head = Base(fromJson: headJson)
		}
		htmlUrl = json["html_url"].stringValue
		id = json["id"].intValue
		issueUrl = json["issue_url"].stringValue
		locked = json["locked"].boolValue
		mergeCommitSha = json["merge_commit_sha"].stringValue
		mergedAt = json["merged_at"].stringValue
		milestone = json["milestone"].stringValue
		number = json["number"].intValue
		patchUrl = json["patch_url"].stringValue
		reviewCommentUrl = json["review_comment_url"].stringValue
		reviewCommentsUrl = json["review_comments_url"].stringValue
		state = json["state"].stringValue
		statusesUrl = json["statuses_url"].stringValue
		title = json["title"].stringValue
		updatedAt = json["updated_at"].stringValue
		url = json["url"].stringValue
		let userJson = json["user"]
		if userJson != JSON.null{
			user = Owner(fromJson: userJson)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if assignee != nil{
			dictionary["assignee"] = assignee
		}
		if base != nil{
			dictionary["base"] = base.toDictionary()
		}
		if body != nil{
			dictionary["body"] = body
		}
		if closedAt != nil{
			dictionary["closed_at"] = closedAt
		}
		if commentsUrl != nil{
			dictionary["comments_url"] = commentsUrl
		}
		if commitsUrl != nil{
			dictionary["commits_url"] = commitsUrl
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if diffUrl != nil{
			dictionary["diff_url"] = diffUrl
		}
		if head != nil{
			dictionary["head"] = head.toDictionary()
		}
		if htmlUrl != nil{
			dictionary["html_url"] = htmlUrl
		}
		if id != nil{
			dictionary["id"] = id
		}
		if issueUrl != nil{
			dictionary["issue_url"] = issueUrl
		}
		if locked != nil{
			dictionary["locked"] = locked
		}
		if mergeCommitSha != nil{
			dictionary["merge_commit_sha"] = mergeCommitSha
		}
		if mergedAt != nil{
			dictionary["merged_at"] = mergedAt
		}
		if milestone != nil{
			dictionary["milestone"] = milestone
		}
		if number != nil{
			dictionary["number"] = number
		}
		if patchUrl != nil{
			dictionary["patch_url"] = patchUrl
		}
		if reviewCommentUrl != nil{
			dictionary["review_comment_url"] = reviewCommentUrl
		}
		if reviewCommentsUrl != nil{
			dictionary["review_comments_url"] = reviewCommentsUrl
		}
		if state != nil{
			dictionary["state"] = state
		}
		if statusesUrl != nil{
			dictionary["statuses_url"] = statusesUrl
		}
		if title != nil{
			dictionary["title"] = title
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if url != nil{
			dictionary["url"] = url
		}
		if user != nil{
			dictionary["user"] = user.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required public init(coder aDecoder: NSCoder)
	{
         base = aDecoder.decodeObjectForKey("base") as? Base
         body = aDecoder.decodeObjectForKey("body") as? String
         commentsUrl = aDecoder.decodeObjectForKey("comments_url") as? String
         commitsUrl = aDecoder.decodeObjectForKey("commits_url") as? String
         createdAt = aDecoder.decodeObjectForKey("created_at") as? String
         diffUrl = aDecoder.decodeObjectForKey("diff_url") as? String
         head = aDecoder.decodeObjectForKey("head") as? Base
         htmlUrl = aDecoder.decodeObjectForKey("html_url") as? String
         id = aDecoder.decodeObjectForKey("id") as? Int
         issueUrl = aDecoder.decodeObjectForKey("issue_url") as? String
         locked = aDecoder.decodeObjectForKey("locked") as? Bool
         mergeCommitSha = aDecoder.decodeObjectForKey("merge_commit_sha") as? String
         number = aDecoder.decodeObjectForKey("number") as? Int
         patchUrl = aDecoder.decodeObjectForKey("patch_url") as? String
         reviewCommentUrl = aDecoder.decodeObjectForKey("review_comment_url") as? String
         reviewCommentsUrl = aDecoder.decodeObjectForKey("review_comments_url") as? String
         state = aDecoder.decodeObjectForKey("state") as? String
         statusesUrl = aDecoder.decodeObjectForKey("statuses_url") as? String
         title = aDecoder.decodeObjectForKey("title") as? String
         updatedAt = aDecoder.decodeObjectForKey("updated_at") as? String
         url = aDecoder.decodeObjectForKey("url") as? String
         user = aDecoder.decodeObjectForKey("user") as? Owner

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc public func encodeWithCoder(aCoder: NSCoder)
	{
		if assignee != nil{
			aCoder.encodeObject(assignee, forKey: "assignee")
		}
		if base != nil{
			aCoder.encodeObject(base, forKey: "base")
		}
		if body != nil{
			aCoder.encodeObject(body, forKey: "body")
		}
		if closedAt != nil{
			aCoder.encodeObject(closedAt, forKey: "closed_at")
		}
		if commentsUrl != nil{
			aCoder.encodeObject(commentsUrl, forKey: "comments_url")
		}
		if commitsUrl != nil{
			aCoder.encodeObject(commitsUrl, forKey: "commits_url")
		}
		if createdAt != nil{
			aCoder.encodeObject(createdAt, forKey: "created_at")
		}
		if diffUrl != nil{
			aCoder.encodeObject(diffUrl, forKey: "diff_url")
		}
		if head != nil{
			aCoder.encodeObject(head, forKey: "head")
		}
		if htmlUrl != nil{
			aCoder.encodeObject(htmlUrl, forKey: "html_url")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if issueUrl != nil{
			aCoder.encodeObject(issueUrl, forKey: "issue_url")
		}
		if locked != nil{
			aCoder.encodeObject(locked, forKey: "locked")
		}
		if mergeCommitSha != nil{
			aCoder.encodeObject(mergeCommitSha, forKey: "merge_commit_sha")
		}
		if mergedAt != nil{
			aCoder.encodeObject(mergedAt, forKey: "merged_at")
		}
		if milestone != nil{
			aCoder.encodeObject(milestone, forKey: "milestone")
		}
		if number != nil{
			aCoder.encodeObject(number, forKey: "number")
		}
		if patchUrl != nil{
			aCoder.encodeObject(patchUrl, forKey: "patch_url")
		}
		if reviewCommentUrl != nil{
			aCoder.encodeObject(reviewCommentUrl, forKey: "review_comment_url")
		}
		if reviewCommentsUrl != nil{
			aCoder.encodeObject(reviewCommentsUrl, forKey: "review_comments_url")
		}
		if state != nil{
			aCoder.encodeObject(state, forKey: "state")
		}
		if statusesUrl != nil{
			aCoder.encodeObject(statusesUrl, forKey: "statuses_url")
		}
		if title != nil{
			aCoder.encodeObject(title, forKey: "title")
		}
		if updatedAt != nil{
			aCoder.encodeObject(updatedAt, forKey: "updated_at")
		}
		if url != nil{
			aCoder.encodeObject(url, forKey: "url")
		}
		if user != nil{
			aCoder.encodeObject(user, forKey: "user")
		}

	}

}