//
//	Owner.swift
//
//	Create by Rafael Chei on 31/5/2016
//	Copyright © 2016 Conductor. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


public class Owner : NSObject, NSCoding{

	var avatarUrl : String!
	var eventsUrl : String!
	var followersUrl : String!
	var followingUrl : String!
	var gistsUrl : String!
	var gravatarId : String!
	var htmlUrl : String!
	var id : Int!
	var login : String!
	var organizationsUrl : String!
	var receivedEventsUrl : String!
	var reposUrl : String!
	var siteAdmin : Bool!
	var starredUrl : String!
	var subscriptionsUrl : String!
	var type : String!
	var url : String!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		avatarUrl = json["avatar_url"].stringValue
		eventsUrl = json["events_url"].stringValue
		followersUrl = json["followers_url"].stringValue
		followingUrl = json["following_url"].stringValue
		gistsUrl = json["gists_url"].stringValue
		gravatarId = json["gravatar_id"].stringValue
		htmlUrl = json["html_url"].stringValue
		id = json["id"].intValue
		login = json["login"].stringValue
		organizationsUrl = json["organizations_url"].stringValue
		receivedEventsUrl = json["received_events_url"].stringValue
		reposUrl = json["repos_url"].stringValue
		siteAdmin = json["site_admin"].boolValue
		starredUrl = json["starred_url"].stringValue
		subscriptionsUrl = json["subscriptions_url"].stringValue
		type = json["type"].stringValue
		url = json["url"].stringValue
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if avatarUrl != nil{
			dictionary["avatar_url"] = avatarUrl
		}
		if eventsUrl != nil{
			dictionary["events_url"] = eventsUrl
		}
		if followersUrl != nil{
			dictionary["followers_url"] = followersUrl
		}
		if followingUrl != nil{
			dictionary["following_url"] = followingUrl
		}
		if gistsUrl != nil{
			dictionary["gists_url"] = gistsUrl
		}
		if gravatarId != nil{
			dictionary["gravatar_id"] = gravatarId
		}
		if htmlUrl != nil{
			dictionary["html_url"] = htmlUrl
		}
		if id != nil{
			dictionary["id"] = id
		}
		if login != nil{
			dictionary["login"] = login
		}
		if organizationsUrl != nil{
			dictionary["organizations_url"] = organizationsUrl
		}
		if receivedEventsUrl != nil{
			dictionary["received_events_url"] = receivedEventsUrl
		}
		if reposUrl != nil{
			dictionary["repos_url"] = reposUrl
		}
		if siteAdmin != nil{
			dictionary["site_admin"] = siteAdmin
		}
		if starredUrl != nil{
			dictionary["starred_url"] = starredUrl
		}
		if subscriptionsUrl != nil{
			dictionary["subscriptions_url"] = subscriptionsUrl
		}
		if type != nil{
			dictionary["type"] = type
		}
		if url != nil{
			dictionary["url"] = url
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required public init(coder aDecoder: NSCoder)
	{
         avatarUrl = aDecoder.decodeObjectForKey("avatar_url") as? String
         eventsUrl = aDecoder.decodeObjectForKey("events_url") as? String
         followersUrl = aDecoder.decodeObjectForKey("followers_url") as? String
         followingUrl = aDecoder.decodeObjectForKey("following_url") as? String
         gistsUrl = aDecoder.decodeObjectForKey("gists_url") as? String
         gravatarId = aDecoder.decodeObjectForKey("gravatar_id") as? String
         htmlUrl = aDecoder.decodeObjectForKey("html_url") as? String
         id = aDecoder.decodeObjectForKey("id") as? Int
         login = aDecoder.decodeObjectForKey("login") as? String
         organizationsUrl = aDecoder.decodeObjectForKey("organizations_url") as? String
         receivedEventsUrl = aDecoder.decodeObjectForKey("received_events_url") as? String
         reposUrl = aDecoder.decodeObjectForKey("repos_url") as? String
         siteAdmin = aDecoder.decodeObjectForKey("site_admin") as? Bool
         starredUrl = aDecoder.decodeObjectForKey("starred_url") as? String
         subscriptionsUrl = aDecoder.decodeObjectForKey("subscriptions_url") as? String
         type = aDecoder.decodeObjectForKey("type") as? String
         url = aDecoder.decodeObjectForKey("url") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc public func encodeWithCoder(aCoder: NSCoder)
	{
		if avatarUrl != nil{
			aCoder.encodeObject(avatarUrl, forKey: "avatar_url")
		}
		if eventsUrl != nil{
			aCoder.encodeObject(eventsUrl, forKey: "events_url")
		}
		if followersUrl != nil{
			aCoder.encodeObject(followersUrl, forKey: "followers_url")
		}
		if followingUrl != nil{
			aCoder.encodeObject(followingUrl, forKey: "following_url")
		}
		if gistsUrl != nil{
			aCoder.encodeObject(gistsUrl, forKey: "gists_url")
		}
		if gravatarId != nil{
			aCoder.encodeObject(gravatarId, forKey: "gravatar_id")
		}
		if htmlUrl != nil{
			aCoder.encodeObject(htmlUrl, forKey: "html_url")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if login != nil{
			aCoder.encodeObject(login, forKey: "login")
		}
		if organizationsUrl != nil{
			aCoder.encodeObject(organizationsUrl, forKey: "organizations_url")
		}
		if receivedEventsUrl != nil{
			aCoder.encodeObject(receivedEventsUrl, forKey: "received_events_url")
		}
		if reposUrl != nil{
			aCoder.encodeObject(reposUrl, forKey: "repos_url")
		}
		if siteAdmin != nil{
			aCoder.encodeObject(siteAdmin, forKey: "site_admin")
		}
		if starredUrl != nil{
			aCoder.encodeObject(starredUrl, forKey: "starred_url")
		}
		if subscriptionsUrl != nil{
			aCoder.encodeObject(subscriptionsUrl, forKey: "subscriptions_url")
		}
		if type != nil{
			aCoder.encodeObject(type, forKey: "type")
		}
		if url != nil{
			aCoder.encodeObject(url, forKey: "url")
		}

	}

}