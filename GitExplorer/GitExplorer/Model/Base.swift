//
//	Base.swift
//
//	Create by Rafael Chei on 31/5/2016
//	Copyright © 2016 Conductor. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON

class Base : NSObject, NSCoding{

	var label : String!
	var ref : String!
	var repo : Item!
	var sha : String!
	var user : Owner!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		label = json["label"].stringValue
		ref = json["ref"].stringValue
		let repoJson = json["repo"]
		if repoJson != JSON.null{
			repo = Item(fromJson: repoJson)
		}
		sha = json["sha"].stringValue
		let userJson = json["user"]
		if userJson != JSON.null{
			user = Owner(fromJson: userJson)
		}
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if label != nil{
			dictionary["label"] = label
		}
		if ref != nil{
			dictionary["ref"] = ref
		}
		if repo != nil{
			dictionary["repo"] = repo.toDictionary()
		}
		if sha != nil{
			dictionary["sha"] = sha
		}
		if user != nil{
			dictionary["user"] = user.toDictionary()
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         label = aDecoder.decodeObjectForKey("label") as? String
         ref = aDecoder.decodeObjectForKey("ref") as? String
         repo = aDecoder.decodeObjectForKey("repo") as? Item
         sha = aDecoder.decodeObjectForKey("sha") as? String
         user = aDecoder.decodeObjectForKey("user") as? Owner

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encodeWithCoder(aCoder: NSCoder)
	{
		if label != nil{
			aCoder.encodeObject(label, forKey: "label")
		}
		if ref != nil{
			aCoder.encodeObject(ref, forKey: "ref")
		}
		if repo != nil{
			aCoder.encodeObject(repo, forKey: "repo")
		}
		if sha != nil{
			aCoder.encodeObject(sha, forKey: "sha")
		}
		if user != nil{
			aCoder.encodeObject(user, forKey: "user")
		}

	}

}