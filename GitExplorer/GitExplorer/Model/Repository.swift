//
//	Repository.swift
//
//	Create by Rafael Chei on 31/5/2016
//	Copyright © 2016 Conductor. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


public class Repository : NSObject, NSCoding{

	var incompleteResults : Bool!
	var items : [Item]!
	var totalCount : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		incompleteResults = json["incomplete_results"].boolValue
		items = [Item]()
		let itemsArray = json["items"].arrayValue
		for itemsJson in itemsArray{
			let value = Item(fromJson: itemsJson)
			items.append(value)
		}
		totalCount = json["total_count"].intValue
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if incompleteResults != nil{
			dictionary["incomplete_results"] = incompleteResults
		}
		if items != nil{
			var dictionaryElements = [NSDictionary]()
			for itemsElement in items {
				dictionaryElements.append(itemsElement.toDictionary())
			}
			dictionary["items"] = dictionaryElements
		}
		if totalCount != nil{
			dictionary["total_count"] = totalCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required public init(coder aDecoder: NSCoder)
	{
         incompleteResults = aDecoder.decodeObjectForKey("incomplete_results") as? Bool
         items = aDecoder.decodeObjectForKey("items") as? [Item]
         totalCount = aDecoder.decodeObjectForKey("total_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc public func encodeWithCoder(aCoder: NSCoder)
	{
		if incompleteResults != nil{
			aCoder.encodeObject(incompleteResults, forKey: "incomplete_results")
		}
		if items != nil{
			aCoder.encodeObject(items, forKey: "items")
		}
		if totalCount != nil{
			aCoder.encodeObject(totalCount, forKey: "total_count")
		}

	}

}