//
//	Item.swift
//
//	Create by Rafael Chei on 31/5/2016
//	Copyright © 2016 Conductor. All rights reserved.
//	Model file Generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation
import SwiftyJSON


public class Item : NSObject, NSCoding{

	var archiveUrl : String!
	var assigneesUrl : String!
	var blobsUrl : String!
	var branchesUrl : String!
	var cloneUrl : String!
	var collaboratorsUrl : String!
	var commentsUrl : String!
	var commitsUrl : String!
	var compareUrl : String!
	var contentsUrl : String!
	var contributorsUrl : String!
	var createdAt : String!
	var defaultBranch : String!
	var deploymentsUrl : String!
	var descriptionField : String!
	var downloadsUrl : String!
	var eventsUrl : String!
	var fork : Bool!
	var forks : Int!
	var forksCount : Int!
	var forksUrl : String!
	var fullName : String!
	var gitCommitsUrl : String!
	var gitRefsUrl : String!
	var gitTagsUrl : String!
	var gitUrl : String!
	var hasDownloads : Bool!
	var hasIssues : Bool!
	var hasPages : Bool!
	var hasWiki : Bool!
	var homepage : String!
	var hooksUrl : String!
	var htmlUrl : String!
	var id : Int!
	var issueCommentUrl : String!
	var issueEventsUrl : String!
	var issuesUrl : String!
	var keysUrl : String!
	var labelsUrl : String!
	var language : String!
	var languagesUrl : String!
	var mergesUrl : String!
	var milestonesUrl : String!
	var mirrorUrl : AnyObject!
	var name : String!
	var notificationsUrl : String!
	var openIssues : Int!
	var openIssuesCount : Int!
	var owner : Owner!
	var privateField : Bool!
	var pullsUrl : String!
	var pushedAt : String!
	var releasesUrl : String!
	var score : Float!
	var size : Int!
	var sshUrl : String!
	var stargazersCount : Int!
	var stargazersUrl : String!
	var statusesUrl : String!
	var subscribersUrl : String!
	var subscriptionUrl : String!
	var svnUrl : String!
	var tagsUrl : String!
	var teamsUrl : String!
	var treesUrl : String!
	var updatedAt : String!
	var url : String!
	var watchers : Int!
	var watchersCount : Int!


	/**
	 * Instantiate the instance using the passed json values to set the properties values
	 */
	init(fromJson json: JSON!){
		if json == nil{
			return
		}
		archiveUrl = json["archive_url"].stringValue
		assigneesUrl = json["assignees_url"].stringValue
		blobsUrl = json["blobs_url"].stringValue
		branchesUrl = json["branches_url"].stringValue
		cloneUrl = json["clone_url"].stringValue
		collaboratorsUrl = json["collaborators_url"].stringValue
		commentsUrl = json["comments_url"].stringValue
		commitsUrl = json["commits_url"].stringValue
		compareUrl = json["compare_url"].stringValue
		contentsUrl = json["contents_url"].stringValue
		contributorsUrl = json["contributors_url"].stringValue
		createdAt = json["created_at"].stringValue
		defaultBranch = json["default_branch"].stringValue
		deploymentsUrl = json["deployments_url"].stringValue
		descriptionField = json["description"].stringValue
		downloadsUrl = json["downloads_url"].stringValue
		eventsUrl = json["events_url"].stringValue
		fork = json["fork"].boolValue
		forks = json["forks"].intValue
		forksCount = json["forks_count"].intValue
		forksUrl = json["forks_url"].stringValue
		fullName = json["full_name"].stringValue
		gitCommitsUrl = json["git_commits_url"].stringValue
		gitRefsUrl = json["git_refs_url"].stringValue
		gitTagsUrl = json["git_tags_url"].stringValue
		gitUrl = json["git_url"].stringValue
		hasDownloads = json["has_downloads"].boolValue
		hasIssues = json["has_issues"].boolValue
		hasPages = json["has_pages"].boolValue
		hasWiki = json["has_wiki"].boolValue
		homepage = json["homepage"].stringValue
		hooksUrl = json["hooks_url"].stringValue
		htmlUrl = json["html_url"].stringValue
		id = json["id"].intValue
		issueCommentUrl = json["issue_comment_url"].stringValue
		issueEventsUrl = json["issue_events_url"].stringValue
		issuesUrl = json["issues_url"].stringValue
		keysUrl = json["keys_url"].stringValue
		labelsUrl = json["labels_url"].stringValue
		language = json["language"].stringValue
		languagesUrl = json["languages_url"].stringValue
		mergesUrl = json["merges_url"].stringValue
		milestonesUrl = json["milestones_url"].stringValue
		mirrorUrl = json["mirror_url"].stringValue
		name = json["name"].stringValue
		notificationsUrl = json["notifications_url"].stringValue
		openIssues = json["open_issues"].intValue
		openIssuesCount = json["open_issues_count"].intValue
		let ownerJson = json["owner"]
		if ownerJson != JSON.null{
			owner = Owner(fromJson: ownerJson)
		}
		privateField = json["private"].boolValue
		pullsUrl = json["pulls_url"].stringValue
		pushedAt = json["pushed_at"].stringValue
		releasesUrl = json["releases_url"].stringValue
		score = json["score"].floatValue
		size = json["size"].intValue
		sshUrl = json["ssh_url"].stringValue
		stargazersCount = json["stargazers_count"].intValue
		stargazersUrl = json["stargazers_url"].stringValue
		statusesUrl = json["statuses_url"].stringValue
		subscribersUrl = json["subscribers_url"].stringValue
		subscriptionUrl = json["subscription_url"].stringValue
		svnUrl = json["svn_url"].stringValue
		tagsUrl = json["tags_url"].stringValue
		teamsUrl = json["teams_url"].stringValue
		treesUrl = json["trees_url"].stringValue
		updatedAt = json["updated_at"].stringValue
		url = json["url"].stringValue
		watchers = json["watchers"].intValue
		watchersCount = json["watchers_count"].intValue
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if archiveUrl != nil{
			dictionary["archive_url"] = archiveUrl
		}
		if assigneesUrl != nil{
			dictionary["assignees_url"] = assigneesUrl
		}
		if blobsUrl != nil{
			dictionary["blobs_url"] = blobsUrl
		}
		if branchesUrl != nil{
			dictionary["branches_url"] = branchesUrl
		}
		if cloneUrl != nil{
			dictionary["clone_url"] = cloneUrl
		}
		if collaboratorsUrl != nil{
			dictionary["collaborators_url"] = collaboratorsUrl
		}
		if commentsUrl != nil{
			dictionary["comments_url"] = commentsUrl
		}
		if commitsUrl != nil{
			dictionary["commits_url"] = commitsUrl
		}
		if compareUrl != nil{
			dictionary["compare_url"] = compareUrl
		}
		if contentsUrl != nil{
			dictionary["contents_url"] = contentsUrl
		}
		if contributorsUrl != nil{
			dictionary["contributors_url"] = contributorsUrl
		}
		if createdAt != nil{
			dictionary["created_at"] = createdAt
		}
		if defaultBranch != nil{
			dictionary["default_branch"] = defaultBranch
		}
		if deploymentsUrl != nil{
			dictionary["deployments_url"] = deploymentsUrl
		}
		if descriptionField != nil{
			dictionary["description"] = descriptionField
		}
		if downloadsUrl != nil{
			dictionary["downloads_url"] = downloadsUrl
		}
		if eventsUrl != nil{
			dictionary["events_url"] = eventsUrl
		}
		if fork != nil{
			dictionary["fork"] = fork
		}
		if forks != nil{
			dictionary["forks"] = forks
		}
		if forksCount != nil{
			dictionary["forks_count"] = forksCount
		}
		if forksUrl != nil{
			dictionary["forks_url"] = forksUrl
		}
		if fullName != nil{
			dictionary["full_name"] = fullName
		}
		if gitCommitsUrl != nil{
			dictionary["git_commits_url"] = gitCommitsUrl
		}
		if gitRefsUrl != nil{
			dictionary["git_refs_url"] = gitRefsUrl
		}
		if gitTagsUrl != nil{
			dictionary["git_tags_url"] = gitTagsUrl
		}
		if gitUrl != nil{
			dictionary["git_url"] = gitUrl
		}
		if hasDownloads != nil{
			dictionary["has_downloads"] = hasDownloads
		}
		if hasIssues != nil{
			dictionary["has_issues"] = hasIssues
		}
		if hasPages != nil{
			dictionary["has_pages"] = hasPages
		}
		if hasWiki != nil{
			dictionary["has_wiki"] = hasWiki
		}
		if homepage != nil{
			dictionary["homepage"] = homepage
		}
		if hooksUrl != nil{
			dictionary["hooks_url"] = hooksUrl
		}
		if htmlUrl != nil{
			dictionary["html_url"] = htmlUrl
		}
		if id != nil{
			dictionary["id"] = id
		}
		if issueCommentUrl != nil{
			dictionary["issue_comment_url"] = issueCommentUrl
		}
		if issueEventsUrl != nil{
			dictionary["issue_events_url"] = issueEventsUrl
		}
		if issuesUrl != nil{
			dictionary["issues_url"] = issuesUrl
		}
		if keysUrl != nil{
			dictionary["keys_url"] = keysUrl
		}
		if labelsUrl != nil{
			dictionary["labels_url"] = labelsUrl
		}
		if language != nil{
			dictionary["language"] = language
		}
		if languagesUrl != nil{
			dictionary["languages_url"] = languagesUrl
		}
		if mergesUrl != nil{
			dictionary["merges_url"] = mergesUrl
		}
		if milestonesUrl != nil{
			dictionary["milestones_url"] = milestonesUrl
		}
		if mirrorUrl != nil{
			dictionary["mirror_url"] = mirrorUrl
		}
		if name != nil{
			dictionary["name"] = name
		}
		if notificationsUrl != nil{
			dictionary["notifications_url"] = notificationsUrl
		}
		if openIssues != nil{
			dictionary["open_issues"] = openIssues
		}
		if openIssuesCount != nil{
			dictionary["open_issues_count"] = openIssuesCount
		}
		if owner != nil{
			dictionary["owner"] = owner.toDictionary()
		}
		if privateField != nil{
			dictionary["private"] = privateField
		}
		if pullsUrl != nil{
			dictionary["pulls_url"] = pullsUrl
		}
		if pushedAt != nil{
			dictionary["pushed_at"] = pushedAt
		}
		if releasesUrl != nil{
			dictionary["releases_url"] = releasesUrl
		}
		if score != nil{
			dictionary["score"] = score
		}
		if size != nil{
			dictionary["size"] = size
		}
		if sshUrl != nil{
			dictionary["ssh_url"] = sshUrl
		}
		if stargazersCount != nil{
			dictionary["stargazers_count"] = stargazersCount
		}
		if stargazersUrl != nil{
			dictionary["stargazers_url"] = stargazersUrl
		}
		if statusesUrl != nil{
			dictionary["statuses_url"] = statusesUrl
		}
		if subscribersUrl != nil{
			dictionary["subscribers_url"] = subscribersUrl
		}
		if subscriptionUrl != nil{
			dictionary["subscription_url"] = subscriptionUrl
		}
		if svnUrl != nil{
			dictionary["svn_url"] = svnUrl
		}
		if tagsUrl != nil{
			dictionary["tags_url"] = tagsUrl
		}
		if teamsUrl != nil{
			dictionary["teams_url"] = teamsUrl
		}
		if treesUrl != nil{
			dictionary["trees_url"] = treesUrl
		}
		if updatedAt != nil{
			dictionary["updated_at"] = updatedAt
		}
		if url != nil{
			dictionary["url"] = url
		}
		if watchers != nil{
			dictionary["watchers"] = watchers
		}
		if watchersCount != nil{
			dictionary["watchers_count"] = watchersCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required public init(coder aDecoder: NSCoder)
	{
         archiveUrl = aDecoder.decodeObjectForKey("archive_url") as? String
         assigneesUrl = aDecoder.decodeObjectForKey("assignees_url") as? String
         blobsUrl = aDecoder.decodeObjectForKey("blobs_url") as? String
         branchesUrl = aDecoder.decodeObjectForKey("branches_url") as? String
         cloneUrl = aDecoder.decodeObjectForKey("clone_url") as? String
         collaboratorsUrl = aDecoder.decodeObjectForKey("collaborators_url") as? String
         commentsUrl = aDecoder.decodeObjectForKey("comments_url") as? String
         commitsUrl = aDecoder.decodeObjectForKey("commits_url") as? String
         compareUrl = aDecoder.decodeObjectForKey("compare_url") as? String
         contentsUrl = aDecoder.decodeObjectForKey("contents_url") as? String
         contributorsUrl = aDecoder.decodeObjectForKey("contributors_url") as? String
         createdAt = aDecoder.decodeObjectForKey("created_at") as? String
         defaultBranch = aDecoder.decodeObjectForKey("default_branch") as? String
         deploymentsUrl = aDecoder.decodeObjectForKey("deployments_url") as? String
         descriptionField = aDecoder.decodeObjectForKey("description") as? String
         downloadsUrl = aDecoder.decodeObjectForKey("downloads_url") as? String
         eventsUrl = aDecoder.decodeObjectForKey("events_url") as? String
         fork = aDecoder.decodeObjectForKey("fork") as? Bool
         forks = aDecoder.decodeObjectForKey("forks") as? Int
         forksCount = aDecoder.decodeObjectForKey("forks_count") as? Int
         forksUrl = aDecoder.decodeObjectForKey("forks_url") as? String
         fullName = aDecoder.decodeObjectForKey("full_name") as? String
         gitCommitsUrl = aDecoder.decodeObjectForKey("git_commits_url") as? String
         gitRefsUrl = aDecoder.decodeObjectForKey("git_refs_url") as? String
         gitTagsUrl = aDecoder.decodeObjectForKey("git_tags_url") as? String
         gitUrl = aDecoder.decodeObjectForKey("git_url") as? String
         hasDownloads = aDecoder.decodeObjectForKey("has_downloads") as? Bool
         hasIssues = aDecoder.decodeObjectForKey("has_issues") as? Bool
         hasPages = aDecoder.decodeObjectForKey("has_pages") as? Bool
         hasWiki = aDecoder.decodeObjectForKey("has_wiki") as? Bool
         homepage = aDecoder.decodeObjectForKey("homepage") as? String
         hooksUrl = aDecoder.decodeObjectForKey("hooks_url") as? String
         htmlUrl = aDecoder.decodeObjectForKey("html_url") as? String
         id = aDecoder.decodeObjectForKey("id") as? Int
         issueCommentUrl = aDecoder.decodeObjectForKey("issue_comment_url") as? String
         issueEventsUrl = aDecoder.decodeObjectForKey("issue_events_url") as? String
         issuesUrl = aDecoder.decodeObjectForKey("issues_url") as? String
         keysUrl = aDecoder.decodeObjectForKey("keys_url") as? String
         labelsUrl = aDecoder.decodeObjectForKey("labels_url") as? String
         language = aDecoder.decodeObjectForKey("language") as? String
         languagesUrl = aDecoder.decodeObjectForKey("languages_url") as? String
         mergesUrl = aDecoder.decodeObjectForKey("merges_url") as? String
         milestonesUrl = aDecoder.decodeObjectForKey("milestones_url") as? String
         name = aDecoder.decodeObjectForKey("name") as? String
         notificationsUrl = aDecoder.decodeObjectForKey("notifications_url") as? String
         openIssues = aDecoder.decodeObjectForKey("open_issues") as? Int
         openIssuesCount = aDecoder.decodeObjectForKey("open_issues_count") as? Int
         owner = aDecoder.decodeObjectForKey("owner") as? Owner
         privateField = aDecoder.decodeObjectForKey("private") as? Bool
         pullsUrl = aDecoder.decodeObjectForKey("pulls_url") as? String
         pushedAt = aDecoder.decodeObjectForKey("pushed_at") as? String
         releasesUrl = aDecoder.decodeObjectForKey("releases_url") as? String
         score = aDecoder.decodeObjectForKey("score") as? Float
         size = aDecoder.decodeObjectForKey("size") as? Int
         sshUrl = aDecoder.decodeObjectForKey("ssh_url") as? String
         stargazersCount = aDecoder.decodeObjectForKey("stargazers_count") as? Int
         stargazersUrl = aDecoder.decodeObjectForKey("stargazers_url") as? String
         statusesUrl = aDecoder.decodeObjectForKey("statuses_url") as? String
         subscribersUrl = aDecoder.decodeObjectForKey("subscribers_url") as? String
         subscriptionUrl = aDecoder.decodeObjectForKey("subscription_url") as? String
         svnUrl = aDecoder.decodeObjectForKey("svn_url") as? String
         tagsUrl = aDecoder.decodeObjectForKey("tags_url") as? String
         teamsUrl = aDecoder.decodeObjectForKey("teams_url") as? String
         treesUrl = aDecoder.decodeObjectForKey("trees_url") as? String
         updatedAt = aDecoder.decodeObjectForKey("updated_at") as? String
         url = aDecoder.decodeObjectForKey("url") as? String
         watchers = aDecoder.decodeObjectForKey("watchers") as? Int
         watchersCount = aDecoder.decodeObjectForKey("watchers_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc public func encodeWithCoder(aCoder: NSCoder)
	{
		if archiveUrl != nil{
			aCoder.encodeObject(archiveUrl, forKey: "archive_url")
		}
		if assigneesUrl != nil{
			aCoder.encodeObject(assigneesUrl, forKey: "assignees_url")
		}
		if blobsUrl != nil{
			aCoder.encodeObject(blobsUrl, forKey: "blobs_url")
		}
		if branchesUrl != nil{
			aCoder.encodeObject(branchesUrl, forKey: "branches_url")
		}
		if cloneUrl != nil{
			aCoder.encodeObject(cloneUrl, forKey: "clone_url")
		}
		if collaboratorsUrl != nil{
			aCoder.encodeObject(collaboratorsUrl, forKey: "collaborators_url")
		}
		if commentsUrl != nil{
			aCoder.encodeObject(commentsUrl, forKey: "comments_url")
		}
		if commitsUrl != nil{
			aCoder.encodeObject(commitsUrl, forKey: "commits_url")
		}
		if compareUrl != nil{
			aCoder.encodeObject(compareUrl, forKey: "compare_url")
		}
		if contentsUrl != nil{
			aCoder.encodeObject(contentsUrl, forKey: "contents_url")
		}
		if contributorsUrl != nil{
			aCoder.encodeObject(contributorsUrl, forKey: "contributors_url")
		}
		if createdAt != nil{
			aCoder.encodeObject(createdAt, forKey: "created_at")
		}
		if defaultBranch != nil{
			aCoder.encodeObject(defaultBranch, forKey: "default_branch")
		}
		if deploymentsUrl != nil{
			aCoder.encodeObject(deploymentsUrl, forKey: "deployments_url")
		}
		if descriptionField != nil{
			aCoder.encodeObject(descriptionField, forKey: "description")
		}
		if downloadsUrl != nil{
			aCoder.encodeObject(downloadsUrl, forKey: "downloads_url")
		}
		if eventsUrl != nil{
			aCoder.encodeObject(eventsUrl, forKey: "events_url")
		}
		if fork != nil{
			aCoder.encodeObject(fork, forKey: "fork")
		}
		if forks != nil{
			aCoder.encodeObject(forks, forKey: "forks")
		}
		if forksCount != nil{
			aCoder.encodeObject(forksCount, forKey: "forks_count")
		}
		if forksUrl != nil{
			aCoder.encodeObject(forksUrl, forKey: "forks_url")
		}
		if fullName != nil{
			aCoder.encodeObject(fullName, forKey: "full_name")
		}
		if gitCommitsUrl != nil{
			aCoder.encodeObject(gitCommitsUrl, forKey: "git_commits_url")
		}
		if gitRefsUrl != nil{
			aCoder.encodeObject(gitRefsUrl, forKey: "git_refs_url")
		}
		if gitTagsUrl != nil{
			aCoder.encodeObject(gitTagsUrl, forKey: "git_tags_url")
		}
		if gitUrl != nil{
			aCoder.encodeObject(gitUrl, forKey: "git_url")
		}
		if hasDownloads != nil{
			aCoder.encodeObject(hasDownloads, forKey: "has_downloads")
		}
		if hasIssues != nil{
			aCoder.encodeObject(hasIssues, forKey: "has_issues")
		}
		if hasPages != nil{
			aCoder.encodeObject(hasPages, forKey: "has_pages")
		}
		if hasWiki != nil{
			aCoder.encodeObject(hasWiki, forKey: "has_wiki")
		}
		if homepage != nil{
			aCoder.encodeObject(homepage, forKey: "homepage")
		}
		if hooksUrl != nil{
			aCoder.encodeObject(hooksUrl, forKey: "hooks_url")
		}
		if htmlUrl != nil{
			aCoder.encodeObject(htmlUrl, forKey: "html_url")
		}
		if id != nil{
			aCoder.encodeObject(id, forKey: "id")
		}
		if issueCommentUrl != nil{
			aCoder.encodeObject(issueCommentUrl, forKey: "issue_comment_url")
		}
		if issueEventsUrl != nil{
			aCoder.encodeObject(issueEventsUrl, forKey: "issue_events_url")
		}
		if issuesUrl != nil{
			aCoder.encodeObject(issuesUrl, forKey: "issues_url")
		}
		if keysUrl != nil{
			aCoder.encodeObject(keysUrl, forKey: "keys_url")
		}
		if labelsUrl != nil{
			aCoder.encodeObject(labelsUrl, forKey: "labels_url")
		}
		if language != nil{
			aCoder.encodeObject(language, forKey: "language")
		}
		if languagesUrl != nil{
			aCoder.encodeObject(languagesUrl, forKey: "languages_url")
		}
		if mergesUrl != nil{
			aCoder.encodeObject(mergesUrl, forKey: "merges_url")
		}
		if milestonesUrl != nil{
			aCoder.encodeObject(milestonesUrl, forKey: "milestones_url")
		}
		if mirrorUrl != nil{
			aCoder.encodeObject(mirrorUrl, forKey: "mirror_url")
		}
		if name != nil{
			aCoder.encodeObject(name, forKey: "name")
		}
		if notificationsUrl != nil{
			aCoder.encodeObject(notificationsUrl, forKey: "notifications_url")
		}
		if openIssues != nil{
			aCoder.encodeObject(openIssues, forKey: "open_issues")
		}
		if openIssuesCount != nil{
			aCoder.encodeObject(openIssuesCount, forKey: "open_issues_count")
		}
		if owner != nil{
			aCoder.encodeObject(owner, forKey: "owner")
		}
		if privateField != nil{
			aCoder.encodeObject(privateField, forKey: "private")
		}
		if pullsUrl != nil{
			aCoder.encodeObject(pullsUrl, forKey: "pulls_url")
		}
		if pushedAt != nil{
			aCoder.encodeObject(pushedAt, forKey: "pushed_at")
		}
		if releasesUrl != nil{
			aCoder.encodeObject(releasesUrl, forKey: "releases_url")
		}
		if score != nil{
			aCoder.encodeObject(score, forKey: "score")
		}
		if size != nil{
			aCoder.encodeObject(size, forKey: "size")
		}
		if sshUrl != nil{
			aCoder.encodeObject(sshUrl, forKey: "ssh_url")
		}
		if stargazersCount != nil{
			aCoder.encodeObject(stargazersCount, forKey: "stargazers_count")
		}
		if stargazersUrl != nil{
			aCoder.encodeObject(stargazersUrl, forKey: "stargazers_url")
		}
		if statusesUrl != nil{
			aCoder.encodeObject(statusesUrl, forKey: "statuses_url")
		}
		if subscribersUrl != nil{
			aCoder.encodeObject(subscribersUrl, forKey: "subscribers_url")
		}
		if subscriptionUrl != nil{
			aCoder.encodeObject(subscriptionUrl, forKey: "subscription_url")
		}
		if svnUrl != nil{
			aCoder.encodeObject(svnUrl, forKey: "svn_url")
		}
		if tagsUrl != nil{
			aCoder.encodeObject(tagsUrl, forKey: "tags_url")
		}
		if teamsUrl != nil{
			aCoder.encodeObject(teamsUrl, forKey: "teams_url")
		}
		if treesUrl != nil{
			aCoder.encodeObject(treesUrl, forKey: "trees_url")
		}
		if updatedAt != nil{
			aCoder.encodeObject(updatedAt, forKey: "updated_at")
		}
		if url != nil{
			aCoder.encodeObject(url, forKey: "url")
		}
		if watchers != nil{
			aCoder.encodeObject(watchers, forKey: "watchers")
		}
		if watchersCount != nil{
			aCoder.encodeObject(watchersCount, forKey: "watchers_count")
		}

	}

}