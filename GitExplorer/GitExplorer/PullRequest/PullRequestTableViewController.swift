//
//  PullRequestTableViewController.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit
import SDWebImage
import NSDate_TimeAgo
import TimeAgoInWords

class PullRequestTableViewController: UITableViewController {

    var repositorio: Item!
    var pullRequests : [PullRequest]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = repositorio.name
        self.navigationController!.navigationBar.topItem!.title = "";
        self.tableView.sectionHeaderHeight = 40
        
        pullRequests = [PullRequest]()
        self.getGithubPullRequests()
    }
    
    func getGithubPullRequests() -> Void {
        let progressHUD = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        progressHUD.label.text = "Buscando PullRequests..."
        ClientGitApi.getPullRequest(repositorio.pullsUrl.stringByReplacingOccurrencesOfString("{/number}", withString: ""))
        {(result: [PullRequest]) in
            self.pullRequests.appendContentsOf(result)
            self.tableView.reloadData()
            progressHUD.hideAnimated(true)
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PullRequestCell", forIndexPath: indexPath) as! PullRequestTableViewCell

        let pullRequest = pullRequests[indexPath.row]
        
        cell.labelTitulo.text = pullRequest.title
        cell.labelDescricao.text = pullRequest.body
        cell.labelUsername.text = pullRequest.user.login
        
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        let date = formatter.dateFromString(pullRequest.createdAt)
        cell.labelNome.text = date?.timeAgo()

        cell.imagemUser.layer.cornerRadius = cell.imagemUser.frame.size.width / 2
        cell.imagemUser.clipsToBounds = true
        SDWebImageManager.sharedManager().downloadImageWithURL(NSURL(string: pullRequest.user.avatarUrl),
                                                               options: [],
                                                               progress: nil,
                                                               completed: {(image, error, cached, finished, url) in
                                                                cell.imagemUser.image = image
        })

        return cell
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = UIColor.whiteColor()
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        label.textAlignment = .Center
        label.font = UIFont.init(name: "HelveticaNeue-Thin", size: 14.0)
        label.text = "\(repositorio.openIssuesCount) opened"
        label.textColor = UIColor.backgroundMenuColor()
        
        view.addSubview(label)
        
        return view
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let baseURL = NSURL(string: pullRequests[indexPath.row].htmlUrl)
        UIApplication.sharedApplication().openURL(baseURL!)
    }
}
