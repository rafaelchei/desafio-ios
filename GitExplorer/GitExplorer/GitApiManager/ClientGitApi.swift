//
//  ClientGitApi.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


struct Constants {
    static let repositoryUrl = "https://api.github.com/search/repositories"
}

public class ClientGitApi: NSObject {

    public class func getRepositories(page: Int, completion: (Repository?) -> Void) {
        Alamofire.request(
            .GET,
            Constants.repositoryUrl,
            parameters: ["q": "language:Java", "sort": "stars", "page": page],
            encoding: .URL)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Erro ao buscar repositorios: \(response.result.error)")
                    completion(nil)
                    return
                }
                
                if let data = response.result.value {
                    let json:JSON = JSON(data)
                    completion(Repository(fromJson: json))
                }

                completion(Repository(fromJson: ""))
        }
    }
    
    public class func getPullRequest(pullUrl: String, completion: [PullRequest] -> Void) {
        var pulls = [PullRequest]()
        Alamofire.request(
            .GET,
            pullUrl,
            parameters: [:],
            encoding: .URL)
            .validate()
            .responseJSON { (response) -> Void in
                guard response.result.isSuccess else {
                    print("Erro ao buscar pull requests: \(response.result.error)")
                    completion(pulls)
                    return
                }
                
                if let data = response.result.value {
                    let json:JSON = JSON(data)                    
                    let itemsArray = json.arrayValue
                    for itemsJson in itemsArray{
                        let value = PullRequest(fromJson: itemsJson)
                        pulls.append(value)
                    }
                    completion(pulls)
                }
                
                completion(pulls)
        }
    }
}
