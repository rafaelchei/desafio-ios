//
//  MainViewController.swift
//  GitExplorer
//
//  Created by Rafael Chei on 5/31/16.
//  Copyright © 2016 Rafael Chei. All rights reserved.
//

import UIKit
import AMSlideMenu

class MainViewController: AMSlideMenuMainViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func segueIdentifierForIndexPathInLeftMenu(indexPath: NSIndexPath!) -> String! {
        var identifier: NSString
        
        switch indexPath.row {
        case 0:
            identifier = "repoSegue"
        case 1:
            identifier = "repoSegue"
        default:
            identifier = "repoSegue"
        }
        return identifier as String
    }
    
    override func configureLeftMenuButton(button: UIButton!) {
        var frame = button.frame
        frame.origin = CGPoint(x: 0, y: 0)
        frame.size = CGSize(width: 27, height: 30)
        button.frame = frame
        
        button.setImage(UIImage(named: "Menu-Filled-50"), forState: UIControlState.Normal)
    }
    
    override func leftMenuWidth() -> CGFloat {
        return 270
    }
    
    override func deepnessForLeftMenu() -> Bool {
        return true
    }

}
